﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETMLDataLoader.Exceptions
{
    /// <summary>
    /// This class is responsible for modelling input data exceptions.
    /// </summary>
    public sealed class InputDataException : Exception
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="exceptionMessage">Exception's message.</param>
        public InputDataException(String exceptionMessage) : base(exceptionMessage) { }

        /// <summary>
        /// Constructor with inner exception.
        /// </summary>
        /// <param name="exceptionMessage">Exception's message.</param>
        /// <param name="innerException">Nested exception.</param>
        public InputDataException(String exceptionMessage, Exception innerException) : base(exceptionMessage, innerException) { }
    }
}
