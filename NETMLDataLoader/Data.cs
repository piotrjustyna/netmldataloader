﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NETMLDataLoader
{
    /// <summary>
    /// Represents the data structure.
    /// Each data entry consists of a combination of multiple features and one output value.
    /// </summary>
    public sealed class Data
    {
        private Dictionary<Int32, Double> _outputs { get; set; }

        private Dictionary<String, Dictionary<Int32, Double>> _features { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Data()
        {
            _outputs = new Dictionary<Int32, Double>();
            _features = new Dictionary<String, Dictionary<Int32, Double>>();
        }

        /// <summary>
        /// Clears the data object.
        /// </summary>
        public void Clear()
        {
            _outputs.Clear();

            foreach (KeyValuePair<String, Dictionary<Int32, Double>> singleXCollection in _features)
            {
                singleXCollection.Value.Clear();
            }

            _features.Clear();
        }

        /// <summary>
        /// Sets value of a feature.
        /// </summary>
        /// <param name="featureKey">Name of the feature, e.g. "x1", "x2", etc.</param>
        /// <param name="index">Index of the feature's data entry. Cannot be negative.</param>
        /// <param name="value">Feature's new value.</param>
        /// <exception cref="ArgumentException">
        /// Thrown when:
        /// - feature key is null, empty or consists of whitespaces
        /// - index is negative
        /// </exception>
        public void SetFeatureValue(String featureKey, Int32 index, Double value)
        {
            if (IsFeatureKeyEmpty(featureKey))
            {
                throw new ArgumentException("Feature key cannot be null, empty or consist of white spaces.");
            }
            else
            {
                if (index >= 0)
                {
                    if (!_features.ContainsKey(featureKey))
                    {
                        _features.Add(featureKey, new Dictionary<Int32, Double>());
                    }
                    else
                    {
                        // collection of features already contain the key, there's no need to add it.
                    }

                    _features[featureKey][index] = value;
                }
                else
                {
                    throw new ArgumentException("Feature index cannot be negative.");
                }
            }
        }

        /// <summary>
        /// Sets value of the output for a given data entry.
        /// </summary>
        /// <param name="index">Index identifying a data entry. Cannot be negative</param>
        /// <param name="value">Output's new value.</param>
        /// <exception cref="ArgumentException">
        /// Thrown when:
        /// - index is negative
        /// </exception>
        public void SetOutputValue(Int32 index, Double value)
        {
            if (index >= 0)
            {
                _outputs[index] = value;
            }
            else
            {
                throw new ArgumentException("Index cannot be negative.");
            }
        }

        /// <summary>
        /// Gets an output value by its index.
        /// </summary>
        /// <param name="index">Index of the output value's data entry.</param>
        /// <returns>Output value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown when:
        /// - index is negative
        /// </exception>
        public Double GetOutputValue(Int32 index)
        {
            Double returnedValue = 0.0;

            if (index >= 0)
            {
                returnedValue = _outputs[index];
            }
            else
            {
                throw new ArgumentException("Index cannot be negative.");
            }

            return returnedValue;
        }

        /// <summary>
        /// Gets a feature name by its name and index.
        /// </summary>
        /// <param name="featureKey">Feature key, e.g. "x1", "x2", etc.</param>
        /// <param name="index">Index of the feature's data entry.</param>
        /// <returns>Feature value.</returns>
        /// <exception cref="ArgumentException">
        /// Thrown when:
        /// - feature key is null, empty or consists of whitespaces
        /// - index is negative
        /// - feature key could not be found
        /// </exception>
        public Double GetFeatureValue(String featureKey, Int32 index)
        {
            Double returnedValue = 0.0;

            if (IsFeatureKeyEmpty(featureKey))
            {
                throw new ArgumentException("Feature key cannot be null, empty or consist of white spaces.");
            }
            else
            {
                if (index >= 0)
                {
                    if (_features.ContainsKey(featureKey))
                    {
                        returnedValue = _features[featureKey][index];
                    }
                    else
                    {
                        throw new ArgumentException(String.Format(
                            "Feature {0} could not be found."
                            , featureKey));
                    }
                }
                else
                {
                    throw new ArgumentException("Feature index cannot be negative.");
                }
            }

            return returnedValue;
        }

        /// <summary>
        /// Returns names of all features.
        /// </summary>
        /// <returns>
        /// Collection of data's feature names.
        /// </returns>
        public String[] GetAllFeatureNames()
        {
            return _features.Keys.ToArray();
        }

        /// <summary>
        /// Gets the number of features in the data object.
        /// </summary>
        /// <returns>Number of features.</returns>
        public Int32 GetNumberOfFeatures()
        {
            return _features.Keys.Count;
        }

        /// <summary>
        /// Gets the number of data entries in the data object.
        /// </summary>
        /// <returns>Number of data entries.</returns>
        public Int32 GetNumberOfDataEntries()
        {
            return _outputs.Count;
        }

        /// <summary>
        /// Helper method checking if a feature key is empty.
        /// </summary>
        /// <param name="featureKey">Feature key to be checked.</param>
        /// <returns>
        /// True - feature key is empty.
        /// False - feature key is not empty.
        /// </returns>
        private Boolean IsFeatureKeyEmpty(String featureKey)
        {
            return String.IsNullOrWhiteSpace(featureKey);
        }
    }
}
