﻿using NETMLDataLoader.Exceptions;
using System;
using System.IO;
using System.Linq;

namespace NETMLDataLoader
{
    /// <summary>
    /// Processes CSV files.
    /// </summary>
    public static class CSV
    {
        /// <summary>
        /// Loads the date from a CSV file.
        /// </summary>
        /// <param name="pathToDataFile">Path to the file.</param>
        /// <param name="separator">Chosen CSV file separator.</param>
        /// <returns>Collection of found data values.</returns>
        /// <exception cref="InputDataException">
        /// Thrown when:
        /// - file is too big to process
        /// - first row does not contain any separators
        /// - number of separators is not consistent in the file
        /// - file contains no rows
        /// - file could not be found
        /// </exception>
        /// <exception cref="ArithmeticException">
        /// Thrown when:
        /// - file contains values which could not be converted to type Double
        /// </exception>
        public static Data Load(String pathToDataFile, Char separator)
        {
            Data loadedData = new Data();

            try
            {
                if (File.Exists(pathToDataFile))
                {
                    Int32 foundRowsIndex = 0;
                    Int32 expectedNumberOfSeparators = 0;

                    foreach (String foundRow in File.ReadLines(pathToDataFile))
                    {
                        if (String.IsNullOrWhiteSpace(foundRow))
                        {
                            // Ignoring the empty row
                        }
                        else
                        {
                            if (foundRowsIndex == 0)
                            {
                                // Looking for the number of separators in the first row to have a value to compare the rest of them to.
                                expectedNumberOfSeparators = foundRow.Count(singleCharacter => singleCharacter == separator);

                                if (expectedNumberOfSeparators > 0)
                                {
                                    if (expectedNumberOfSeparators > Consts.MaximumDataFileSeparators)
                                    {
                                        throw new InputDataException(String.Format(
                                            "File is too big to process. Expected maximum number of separators: {0}, found: {1}."
                                            , Consts.MaximumDataFileSeparators
                                            , expectedNumberOfSeparators));
                                    }
                                    else
                                    {
                                        // No problems detected with separators
                                    }
                                }
                                else
                                {
                                    throw new InputDataException("First row doesn't contain any separators, file is invalid.");
                                }
                            }

                            if (foundRowsIndex + 1 <= Consts.MaximumDataFileRows)
                            {
                                if (foundRow.Count(singleCharacter => singleCharacter == separator) == expectedNumberOfSeparators)
                                {
                                    String[] foundValues = foundRow.Split(separator);
                                    Int32? problematicIndex = null;

                                    if (CheckIfValuesCanBeParsedToDouble(foundValues, out problematicIndex))
                                    {
                                        Int32 featureIndex = 1;

                                        foreach (String singleValue in foundValues.Take(foundValues.Count() - 1))   // Last column contains the Y values
                                        {
                                            loadedData.SetFeatureValue(
                                                CreateFeatureKey(featureIndex)
                                                , foundRowsIndex
                                                , Double.Parse(singleValue));  // X1 .. Xn where n is the number of features
                                            featureIndex++;
                                        }

                                        loadedData.SetOutputValue(
                                            foundRowsIndex
                                            , Double.Parse(foundValues.Last()));
                                    }
                                    else
                                    {
                                        loadedData.Clear();

                                        throw new ArithmeticException(String.Format(
                                            "Could not convert file value to Double. Row: {0}, Column: {1}."
                                            , foundRowsIndex + 1
                                            , problematicIndex + 1));
                                    }
                                }
                                else
                                {
                                    loadedData.Clear();

                                    throw new InputDataException("Number of separators in the data file is not consistent, file is invalid.");
                                }

                                foundRowsIndex++;
                            }
                            else
                            {
                                throw new InputDataException(String.Format(
                                    "File is too big to process. Expected maximum number of rows: {0}, found: {1}."
                                    , Consts.MaximumDataFileRows
                                    , foundRowsIndex));
                            }
                        }
                    }

                    if (foundRowsIndex == 0)
                    {
                        throw new InputDataException("File contains no rows.");
                    }
                    else
                    {
                        // File has more than one row, which is correct
                    }
                }
                else
                {
                    throw new InputDataException("File could not be found.");
                }
            }
            catch (ArithmeticException exception)
            {
                throw new ArithmeticException(String.Format("Could not read data file: {0} - parsing error.", pathToDataFile), exception);
            }
            catch (InputDataException exception)
            {
                throw new InputDataException(String.Format("Could not read data file: {0} - input data error.", pathToDataFile), exception);
            }
            catch (Exception exception)
            {
                throw new Exception(String.Format("Could not read data file: {0} - unknown error.", pathToDataFile), exception);
            }

            return loadedData;
        }

        private static String CreateFeatureKey(Int32 featureIndex)
        {
            return String.Format(
                "{0}{1}"
                , Consts.DefaultFeatureName
                , featureIndex);
        }

        private static Boolean CheckIfValuesCanBeParsedToDouble(String[] foundValues
            , out Int32? problematicIndex)
        {
            Boolean result = true;
            problematicIndex = null;

            for (Int32 i = 0;
                i < foundValues.Length
                && result == true;
                i++)
            {
                String singleValue = foundValues[i];
                Double singleParsedValue = 0.0;

                result &= Double.TryParse(
                    singleValue
                    , out singleParsedValue);

                if (!result)
                {
                    problematicIndex = i;
                }
            }

            return result;
        }
    }
}
