﻿using System;
using System.Text;

namespace NETMLDataLoader
{
    internal static class Consts
    {
        internal static readonly Encoding DefaultEncoding = Encoding.UTF8;

        internal const Int32 MaximumDataFileRows = 100000;
        internal const Int32 MaximumDataFileColumns = 100;
        internal const Int32 MaximumDataFileSeparators = MaximumDataFileColumns - 1;
        internal const String DefaultFeatureName = "x";
        internal const String DoubleStringFormat = "G17";
    }
}
