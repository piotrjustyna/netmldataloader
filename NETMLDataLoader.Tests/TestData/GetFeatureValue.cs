﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class GetFeatureValue
    {
        [TestMethod]
        public void GetFeatureValue_WhenUsingNullFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.GetFeatureValue(null, 0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void GetFeatureValue_WhenUsingEmptyFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.GetFeatureValue(String.Empty, 0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void GetFeatureValue_WhenUsingWhitespaceFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.GetFeatureValue("\t   ", 0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void GetFeatureValue_WhenUsingNegativeIndex_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.GetFeatureValue("x1", -1);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature index cannot be negative."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void GetFeatureValue_WhenUsingNonexistentFeatureKey_ShouldThrowAnArgumentException()
        {
            // arrange
            String featureKey = "x1";
            Data foundData = new Data();

            try
            {
                // act
                foundData.GetFeatureValue(featureKey, 1);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains(String.Format("Feature {0} could not be found.", featureKey)));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }
        }

        [TestMethod]
        public void SetFeatureValue_WhenUsingCorrectFeatureKeyAndIndex_ShouldSetFeatureValueCorrectly()
        {
            // act
            String featureKey = "x1";
            Double testedValue = 1.0;
            Data foundData = new Data();
            foundData.SetFeatureValue(featureKey, 1, testedValue);

            // assert
            Assert.AreEqual(foundData.GetFeatureValue(featureKey, 1), testedValue, "Data value not set properly.");
        }
    }
}
