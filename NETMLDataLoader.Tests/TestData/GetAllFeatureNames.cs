﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class GetAllFeatureNames
    {
        [TestMethod]
        public void GetAllFeatureNames_WhenUsingRandomNumberOfFeatures_ShouldReturnCorrectResult()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Data testedData = new Data();
            Int32 testedNumberOfFeatures = randomNumbersGenerator.Next(Consts.MaximumDataFileColumns);
            List<String> allFeatureNames = new List<String>();
            String[] testedFeatureNames = null;
            String featureName = null;

            for (Int32 i = 0; i < testedNumberOfFeatures; i++)
            {
                featureName = Consts.DefaultFeatureName + i;

                allFeatureNames.Add(featureName);
                testedData.SetFeatureValue(featureName, 0, 1.0);
            }

            // act
            testedFeatureNames = testedData.GetAllFeatureNames();

            // assert
            for (Int32 i = 0; i < testedNumberOfFeatures; i++)
            {
                featureName = testedFeatureNames[i];

                Assert.IsTrue(allFeatureNames.Contains(featureName), String.Format("Feature name {0} was not expected to be used.", featureName));
                allFeatureNames.Remove(featureName);
            }

            Assert.AreEqual(allFeatureNames.Count, 0, "Collection of all feature names is expected to be empty.");
        }
    }
}
