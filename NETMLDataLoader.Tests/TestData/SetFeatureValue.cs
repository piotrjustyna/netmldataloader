﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class SetFeatureValue
    {
        [TestMethod]
        public void SetFeatureValue_WhenUsingNullFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.SetFeatureValue(null, 0, 0.0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void SetFeatureValue_WhenUsingEmptyFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.SetFeatureValue(String.Empty, 0, 0.0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void SetFeatureValue_WhenUsingWhitespaceFeatureKey_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.SetFeatureValue("\t   ", 0, 0.0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature key cannot be null, empty or consist of white spaces."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void SetFeatureValue_WhenUsingNegativeIndex_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.SetFeatureValue("x1", -1, 0.0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Feature index cannot be negative."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void SetFeatureValue_WhenUsingCorrectFeatureKeyAndIndex_ShouldSetFeatureValueCorrectly()
        {
            // act
            String featureKey = "x1";
            Data foundData = new Data();
            foundData.SetFeatureValue(featureKey, 1, 1.0);

            // assert
            Assert.AreEqual(foundData.GetFeatureValue(featureKey, 1), 1.0, "Data value not set properly.");
        }
    }
}
