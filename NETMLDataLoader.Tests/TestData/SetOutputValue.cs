﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class SetOutputValue
    {
        [TestMethod]
        public void SetOutputValue_WhenUsingNegativeIndex_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.SetOutputValue(-1, 1.0);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Index cannot be negative."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void SetOutputValue_WhenUsingCorrectIndex_ShouldSetOutputValueCorrectly()
        {
            // act
            Data foundData = new Data();
            foundData.SetOutputValue(0, 1.0);

            Assert.AreEqual(foundData.GetOutputValue(0), 1.0, "Data value not set properly.");
        }
    }
}
