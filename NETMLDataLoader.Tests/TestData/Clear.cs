﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class Clear
    {
        [TestMethod]
        public void Clear_WhenClearingTheDataObject_AllDataEntriesShouldbeDeleted()
        {
            // arrange
            String featureKey1 = "x1";
            String featureKey2 = "x2";
            String featureKey3 = "x3";
            Data testedData = new Data();

            testedData.SetOutputValue(0, 1.0);
            testedData.SetFeatureValue(featureKey1, 0, 2.0);
            testedData.SetFeatureValue(featureKey2, 0, 3.0);
            testedData.SetFeatureValue(featureKey3, 0, 4.0);

            // assert
            Assert.AreEqual(testedData.GetNumberOfFeatures(), 3);
            Assert.AreEqual(testedData.GetNumberOfDataEntries(), 1);

            // act
            testedData.Clear();

            // assert
            Assert.AreEqual(testedData.GetNumberOfFeatures(), 0);
            Assert.AreEqual(testedData.GetNumberOfDataEntries(), 0);
        }
    }
}
