﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class GetOutputValue
    {
        [TestMethod]
        public void GetOutputValue_WhenUsingNegativeIndex_ShouldThrowAnArgumentException()
        {
            try
            {
                // act
                Data foundData = new Data();
                foundData.GetOutputValue(-1);
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Index cannot be negative."));
                Assert.AreEqual(exception.GetType(), typeof(ArgumentException), "Expected \"ArgumentException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void GetOutputValue_WhenUsingCorrectIndex_ShouldReturnCorrectValue()
        {
            // act
            Data foundData = new Data();
            Double valueToBeUsed = 1.0;
            foundData.SetOutputValue(0, valueToBeUsed);

            Assert.AreEqual(foundData.GetOutputValue(0), valueToBeUsed, "Data value not set properly.");
        }
    }
}
