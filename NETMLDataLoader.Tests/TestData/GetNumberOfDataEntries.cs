﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class GetNumberOfDataEntries
    {
        [TestMethod]
        public void GetNumberOfDataEntries_WhenUsingDifferentNumberOfDataEntries_ShouldReturnCorrectResult()
        {
            // arrange
            String featureKey = "x1";
            Data testedData = new Data();
            
            // act
            testedData.SetFeatureValue(featureKey, 0, 1.0);
            testedData.SetOutputValue(0, 1.0);

            // assert
            Assert.AreEqual(testedData.GetNumberOfDataEntries(), 1);

            // act
            testedData.SetFeatureValue(featureKey, 1, 1.0);
            testedData.SetOutputValue(1, 1.0);

            // assert
            Assert.AreEqual(testedData.GetNumberOfDataEntries(), 2);

            // act
            testedData.SetFeatureValue(featureKey, 2, 1.0);
            testedData.SetOutputValue(2, 1.0);

            // assert
            Assert.AreEqual(testedData.GetNumberOfDataEntries(), 3);
        }
    }
}
