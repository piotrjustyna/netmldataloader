﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace NETMLDataLoader.Tests.TestData
{
    [TestClass]
    public class GetNumberOfFeatures
    {
        [TestMethod]
        public void GetNumberOfFeatures_WhenUsingDifferentNumberOfFeatures_ShouldReturnCorrectResult()
        {
            // arrange
            String featureKey1 = "x1";
            String featureKey2 = "x2";
            String featureKey3 = "x3";
            Data testedData = new Data();

            // act
            testedData.SetFeatureValue(featureKey1, 0, 1.0);
            // assert
            Assert.AreEqual(testedData.GetNumberOfFeatures(), 1);

            // act
            testedData.SetFeatureValue(featureKey2, 1, 1.0);
            // assert
            Assert.AreEqual(testedData.GetNumberOfFeatures(), 2);

            // act
            testedData.SetFeatureValue(featureKey3, 2, 1.0);
            // assert
            Assert.AreEqual(testedData.GetNumberOfFeatures(), 3);
        }
    }
}
