﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Globalization;
using System.Text;
using System.IO;
using NETMLDataLoader.Exceptions;

namespace NETMLDataLoader.Tests.TestCSV
{
    [TestClass]
    public class Load
    {
        private String PrepareTestFile()
        {
            String pathToTestFile = "test_" + Guid.NewGuid().ToString();

            using (FileStream stream = File.Create(pathToTestFile)) { }

            return pathToTestFile;
        }

        private void PrepareValidTestFileContent(
            String pathToTestFile
            , Char separator
            , Int32 numberOfRows
            , Int32 numberOfColumns
            , out Data testedData)
        {
            Random randomNumbersGenerator = new Random();
            StringBuilder dataFileContent = new StringBuilder();

            using (FileStream fileWriter = File.OpenWrite(pathToTestFile))
            {
                testedData = new Data();

                for (Int32 rowCounter = 0; rowCounter < numberOfRows; rowCounter++)
                {
                    for (Int32 columnCounter = 0; columnCounter < numberOfColumns; columnCounter++)
                    {
                        Double fileValue = randomNumbersGenerator.NextDouble();

                        dataFileContent.Append(fileValue.ToString("G17"));
                        dataFileContent.Append(separator);

                        if (columnCounter == numberOfColumns - 1)
                        {
                            testedData.SetOutputValue(rowCounter, fileValue);
                        }
                        else
                        {
                            testedData.SetFeatureValue(
                                String.Format(
                                    "{0}{1}"
                                    , Consts.DefaultFeatureName
                                    , columnCounter + 1)
                                , rowCounter
                                , fileValue);
                        }
                    }

                    dataFileContent.Remove(dataFileContent.Length - 1, 1);
                    dataFileContent.AppendLine();

                    fileWriter.Write(Encoding.UTF8.GetBytes(dataFileContent.ToString()), 0, dataFileContent.Length);
                    dataFileContent.Clear();
                }
            }
        }

        private void PrepareInvalidTestFileContentWithInconsistentNumberOfSeparators(
            String pathToTestFile
            , Char separator
            , Int32 numberOfRows
            , Int32 numberOfColumns)
        {
            Random randomNumbersGenerator = new Random();
            StringBuilder dataFileContent = new StringBuilder();

            for (Int32 rowCounter = 0; rowCounter < numberOfRows; rowCounter++)
            {
                for (Int32 columnCounter = 0; columnCounter < numberOfColumns + rowCounter; columnCounter++)
                {
                    dataFileContent.Append(randomNumbersGenerator.NextDouble());
                    dataFileContent.Append(separator);
                }

                dataFileContent.Remove(dataFileContent.Length - 1, 1);
                dataFileContent.AppendLine();
            }

            File.WriteAllText(pathToTestFile, dataFileContent.ToString());
        }

        private void PrepareInvalidTestFileContentWithUnparseableValues(
            String pathToTestFile
            , Char separator
            , Int32 numberOfRows
            , Int32 numberOfColumns)
        {
            StringBuilder dataFileContent = new StringBuilder();

            for (Int32 rowCounter = 0; rowCounter < numberOfRows; rowCounter++)
            {
                for (Int32 columnCounter = 0; columnCounter < numberOfColumns; columnCounter++)
                {
                    dataFileContent.Append("[unparseable_value]");
                    dataFileContent.Append(separator);
                }

                dataFileContent.Remove(dataFileContent.Length - 1, 1);
                dataFileContent.AppendLine();
            }

            File.WriteAllText(pathToTestFile, dataFileContent.ToString());
        }

        private void DeleteTestFile(String pathToTestFile)
        {
            File.Delete(pathToTestFile);
        }

        /// <summary>
        /// Clears all remaning test files (test files are not cleared by individual unit test methods if their assertions fail).
        /// </summary>
        private static void DeleteAllRemainingTestFiles()
        {
            foreach (String singleFileName in Directory.GetFiles(".", "test_*"))
            {
                File.Delete(singleFileName);
            }
        }

        [ClassInitialize]
        public static void PrepareTests(TestContext context)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        }

        [ClassCleanup]
        public static void CleanupTests()
        {
            DeleteAllRemainingTestFiles();
        }

        [TestMethod]
        public void Load_WhenLoadingMissingFile_ShouldThrowAnException()
        {
            try
            {
                // arrange & act
                Data foundData = CSV.Load(Guid.NewGuid().ToString(), ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("File could not be found."), "Expected \"File could not be found.\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingEmptyFile_ShouldThrowAnException()
        {
            // arrange
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("File contains no rows."), "Expected \"File contains no rows.\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingValidFile_ShouldLoadDataCorrectly()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = randomNumbersGenerator.Next(1, Consts.MaximumDataFileRows);
            Int32 numberOfColumns = randomNumbersGenerator.Next(2, Consts.MaximumDataFileColumns + 1);
            String pathToTestFile = PrepareTestFile();
            Data testedData = null;

            PrepareValidTestFileContent(
                pathToTestFile
                , ','
                , numberOfRows
                , numberOfColumns
                , out testedData);

            // act
            Data foundData = CSV.Load(pathToTestFile, ',');

            // assert
            Assert.AreEqual(numberOfRows, foundData.GetNumberOfDataEntries(), "Test file was not loaded correctly - number of read rows does not match the number of file rows.");
            Assert.AreEqual(numberOfColumns, foundData.GetNumberOfFeatures() + 1, "Test file was not loaded correctly - number of read columns does not match the number of file columns.");

            for (Int32 dataFileRowIndex = 0; dataFileRowIndex < testedData.GetNumberOfDataEntries(); dataFileRowIndex++)
            {
                Assert.IsTrue(
                    foundData.GetOutputValue(dataFileRowIndex) == testedData.GetOutputValue(dataFileRowIndex)
                    , String.Format("Loaded output value {0} does not match the initial value {1}."
                        , foundData.GetOutputValue(dataFileRowIndex).ToString(Consts.DoubleStringFormat)
                        , testedData.GetOutputValue(dataFileRowIndex).ToString(Consts.DoubleStringFormat)));

                for (Int32 dataFileFeatureCounter = 1; dataFileFeatureCounter <= testedData.GetNumberOfFeatures(); dataFileFeatureCounter++)
                {
                    Assert.IsTrue(
                        foundData.GetFeatureValue(
                            String.Format("{0}{1}"
                                , Consts.DefaultFeatureName
                                , dataFileFeatureCounter)
                            , dataFileRowIndex)

                        == testedData.GetFeatureValue(
                            String.Format("{0}{1}"
                                , Consts.DefaultFeatureName
                                , dataFileFeatureCounter)
                            , dataFileRowIndex)

                        , String.Format("Loaded feature value {0} does not match the initial feature {1}."
                            , foundData.GetFeatureValue(
                                String.Format("{0}{1}"
                                    , Consts.DefaultFeatureName
                                    , dataFileFeatureCounter)
                                , dataFileRowIndex).ToString(Consts.DoubleStringFormat)

                            , testedData.GetFeatureValue(
                                String.Format("{0}{1}"
                                    , Consts.DefaultFeatureName
                                    , dataFileFeatureCounter)
                                , dataFileRowIndex).ToString(Consts.DoubleStringFormat)));
                }
            }

            // arrange
            DeleteTestFile(pathToTestFile);
        }

        [TestMethod]
        public void Load_WhenLoadingFileWithInconsistentNumberOfSeparators_ShouldThrowAnException()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = randomNumbersGenerator.Next(2, 11);
            Int32 numberOfColumns = randomNumbersGenerator.Next(2, 11);
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;
            PrepareInvalidTestFileContentWithInconsistentNumberOfSeparators(pathToTestFile, ',', numberOfRows, numberOfColumns);

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("Number of separators in the data file is not consistent, file is invalid."), "Expected \"Number of separators in the data file is not consistent, file is invalid.\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingFileWithTooManyRows_ShouldThrowAnException()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = Consts.MaximumDataFileRows + 1;
            Int32 numberOfColumns = randomNumbersGenerator.Next(2, 11);
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;
            Data testedData = null;

            PrepareValidTestFileContent(
                pathToTestFile
                , ','
                , numberOfRows
                , numberOfColumns
                , out testedData);

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("File is too big to process. Expected maximum number of rows:"), "Expected \"File is too big to process. Expected maximum number of rows:\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingFileWithTooManySeparators_ShouldThrowAnException()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = randomNumbersGenerator.Next(1, 11);
            Int32 numberOfColumns = Consts.MaximumDataFileColumns + 1;
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;
            Data testData = null;

            PrepareValidTestFileContent(
                pathToTestFile
                , ','
                , numberOfRows
                , numberOfColumns
                , out testData);

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("File is too big to process. Expected maximum number of separators:"), "Expected \"File is too big to process. Expected maximum number of separators:\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingFileWithNoSeparators_ShouldThrowAnException()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = randomNumbersGenerator.Next(1, 11);
            Int32 numberOfColumns = 1;
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;
            Data testedData = null;

            PrepareValidTestFileContent(
                pathToTestFile
                , ','
                , numberOfRows
                , numberOfColumns
                , out testedData);

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("input data error"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("First row doesn't contain any separators, file is invalid."), "Expected \"First row doesn't contain any separators, file is invalid.\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.InnerException.GetType(), "Expected an \"InputDataException\" exception.");
                Assert.AreEqual(typeof(InputDataException), exception.GetType(), "Expected an \"InputDataException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingUnparseableFile_ShouldThrowAnException()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = randomNumbersGenerator.Next(1, 11);
            Int32 numberOfColumns = randomNumbersGenerator.Next(2, 11);
            String pathToTestFile = PrepareTestFile();
            Data foundData = null;
            PrepareInvalidTestFileContentWithUnparseableValues(pathToTestFile, ',', numberOfRows, numberOfColumns);

            try
            {
                // act
                foundData = CSV.Load(pathToTestFile, ',');
            }
            catch (Exception exception)
            {
                // assert
                Assert.IsTrue(exception.Message.Contains("Could not read data file"), "Caught unexpected exception.");
                Assert.IsTrue(exception.InnerException != null && exception.InnerException.Message.Contains("Could not convert file value to Double."), "Expected \"Could not convert\" exception.");
                Assert.AreEqual(typeof(ArithmeticException), exception.InnerException.GetType(), "Expected an \"ArithmeticException\" exception.");
                Assert.AreEqual(typeof(ArithmeticException), exception.GetType(), "Expected an \"ArithmeticException\" exception.");

                // arrange
                DeleteTestFile(pathToTestFile);
                return;
            }

            // assert
            Assert.Fail("Expected an exception to be thrown.");
        }

        [TestMethod]
        public void Load_WhenLoadingAFileOfMaximumSize_ShouldLoadDataCorrectly()
        {
            // arrange
            Random randomNumbersGenerator = new Random();
            Int32 numberOfRows = Consts.MaximumDataFileRows;
            Int32 numberOfColumns = Consts.MaximumDataFileColumns;
            String pathToTestFile = PrepareTestFile();
            Data testedData = null;

            PrepareValidTestFileContent(
                pathToTestFile
                , ','
                , numberOfRows
                , numberOfColumns
                , out testedData);

            // act
            Data foundData = CSV.Load(pathToTestFile, ',');

            // assert
            Assert.AreEqual(numberOfRows, foundData.GetNumberOfDataEntries(), "Test file was not loaded correctly - number of read rows does not match the number of file rows.");
            Assert.AreEqual(numberOfColumns, foundData.GetNumberOfFeatures() + 1, "Test file was not loaded correctly - number of read columns does not match the number of file columns.");

            // arrange
            DeleteTestFile(pathToTestFile);
        }
    }
}
