﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NETMLDataLoader.Tests")]
[assembly: AssemblyDescription("Tests for NETMLDataLoader - machine learning data loading library for .NET")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Piotr Justyna")]
[assembly: AssemblyProduct("NETMLDataLoader.Tests")]
[assembly: AssemblyCopyright("Copyright © Piotr Justyna 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c4dc3ecd-0a59-43fe-b86d-f15b5268b502")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
